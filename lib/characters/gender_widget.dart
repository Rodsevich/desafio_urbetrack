import 'package:flutter/material.dart';

import 'models/character.dart';

class GenderWidget extends StatelessWidget {
  const GenderWidget({super.key, required this.gender});

  final Gender gender;

  @override
  Widget build(BuildContext context) {
    return switch (gender) {
      Gender.female => Icon(
          Icons.female,
          color: Colors.pink,
        ),
      Gender.male => Icon(
          Icons.male,
          color: Colors.lightBlue,
        ),
      Gender.hermaphrodite => Icon(
          Icons.warning,
          color: Colors.amber,
        ),
      Gender.notAny => Icon(Icons.adf_scanner_outlined),
    };
  }
}
