import 'package:desafio_urbetrack/characters/models/character.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'characters_list_page.freezed.dart';
part 'characters_list_page.g.dart';

@freezed
class CharactersListPage with _$CharactersListPage {
  const factory CharactersListPage({
    required int count,
    required String? next,
    required String? previous,
    required List<Character> results,
  }) = _CharactersListPage;

  factory CharactersListPage.fromJson(Map<String, dynamic> json) => _$CharactersListPageFromJson(json);
}
