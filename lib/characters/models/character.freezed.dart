// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'character.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

Character _$CharacterFromJson(Map<String, dynamic> json) {
  return _Character.fromJson(json);
}

/// @nodoc
mixin _$Character {
  String get name => throw _privateConstructorUsedError;
  String get height => throw _privateConstructorUsedError;
  String get mass => throw _privateConstructorUsedError;
  String? get hairColor => throw _privateConstructorUsedError;
  String? get skinColor => throw _privateConstructorUsedError;
  String? get eyeColor => throw _privateConstructorUsedError;
  String? get birthYear => throw _privateConstructorUsedError; // ignore: invalid_annotation_target
  @JsonKey(fromJson: genderFromJson, toJson: genderToJson)
  Gender get gender => throw _privateConstructorUsedError;
  String get homeworld => throw _privateConstructorUsedError;
  List<String> get films => throw _privateConstructorUsedError;
  List<String> get species => throw _privateConstructorUsedError;
  List<String> get vehicles => throw _privateConstructorUsedError;
  List<String> get starships => throw _privateConstructorUsedError;
  DateTime get created => throw _privateConstructorUsedError;
  DateTime get edited => throw _privateConstructorUsedError;
  String get url => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $CharacterCopyWith<Character> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $CharacterCopyWith<$Res> {
  factory $CharacterCopyWith(Character value, $Res Function(Character) then) = _$CharacterCopyWithImpl<$Res, Character>;
  @useResult
  $Res call(
      {String name,
      String height,
      String mass,
      String? hairColor,
      String? skinColor,
      String? eyeColor,
      String? birthYear,
      @JsonKey(fromJson: genderFromJson, toJson: genderToJson) Gender gender,
      String homeworld,
      List<String> films,
      List<String> species,
      List<String> vehicles,
      List<String> starships,
      DateTime created,
      DateTime edited,
      String url});
}

/// @nodoc
class _$CharacterCopyWithImpl<$Res, $Val extends Character> implements $CharacterCopyWith<$Res> {
  _$CharacterCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? name = null,
    Object? height = null,
    Object? mass = null,
    Object? hairColor = freezed,
    Object? skinColor = freezed,
    Object? eyeColor = freezed,
    Object? birthYear = freezed,
    Object? gender = null,
    Object? homeworld = null,
    Object? films = null,
    Object? species = null,
    Object? vehicles = null,
    Object? starships = null,
    Object? created = null,
    Object? edited = null,
    Object? url = null,
  }) {
    return _then(_value.copyWith(
      name: null == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      height: null == height
          ? _value.height
          : height // ignore: cast_nullable_to_non_nullable
              as String,
      mass: null == mass
          ? _value.mass
          : mass // ignore: cast_nullable_to_non_nullable
              as String,
      hairColor: freezed == hairColor
          ? _value.hairColor
          : hairColor // ignore: cast_nullable_to_non_nullable
              as String?,
      skinColor: freezed == skinColor
          ? _value.skinColor
          : skinColor // ignore: cast_nullable_to_non_nullable
              as String?,
      eyeColor: freezed == eyeColor
          ? _value.eyeColor
          : eyeColor // ignore: cast_nullable_to_non_nullable
              as String?,
      birthYear: freezed == birthYear
          ? _value.birthYear
          : birthYear // ignore: cast_nullable_to_non_nullable
              as String?,
      gender: null == gender
          ? _value.gender
          : gender // ignore: cast_nullable_to_non_nullable
              as Gender,
      homeworld: null == homeworld
          ? _value.homeworld
          : homeworld // ignore: cast_nullable_to_non_nullable
              as String,
      films: null == films
          ? _value.films
          : films // ignore: cast_nullable_to_non_nullable
              as List<String>,
      species: null == species
          ? _value.species
          : species // ignore: cast_nullable_to_non_nullable
              as List<String>,
      vehicles: null == vehicles
          ? _value.vehicles
          : vehicles // ignore: cast_nullable_to_non_nullable
              as List<String>,
      starships: null == starships
          ? _value.starships
          : starships // ignore: cast_nullable_to_non_nullable
              as List<String>,
      created: null == created
          ? _value.created
          : created // ignore: cast_nullable_to_non_nullable
              as DateTime,
      edited: null == edited
          ? _value.edited
          : edited // ignore: cast_nullable_to_non_nullable
              as DateTime,
      url: null == url
          ? _value.url
          : url // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$CharacterImplCopyWith<$Res> implements $CharacterCopyWith<$Res> {
  factory _$$CharacterImplCopyWith(_$CharacterImpl value, $Res Function(_$CharacterImpl) then) =
      __$$CharacterImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {String name,
      String height,
      String mass,
      String? hairColor,
      String? skinColor,
      String? eyeColor,
      String? birthYear,
      @JsonKey(fromJson: genderFromJson, toJson: genderToJson) Gender gender,
      String homeworld,
      List<String> films,
      List<String> species,
      List<String> vehicles,
      List<String> starships,
      DateTime created,
      DateTime edited,
      String url});
}

/// @nodoc
class __$$CharacterImplCopyWithImpl<$Res> extends _$CharacterCopyWithImpl<$Res, _$CharacterImpl>
    implements _$$CharacterImplCopyWith<$Res> {
  __$$CharacterImplCopyWithImpl(_$CharacterImpl _value, $Res Function(_$CharacterImpl) _then) : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? name = null,
    Object? height = null,
    Object? mass = null,
    Object? hairColor = freezed,
    Object? skinColor = freezed,
    Object? eyeColor = freezed,
    Object? birthYear = freezed,
    Object? gender = null,
    Object? homeworld = null,
    Object? films = null,
    Object? species = null,
    Object? vehicles = null,
    Object? starships = null,
    Object? created = null,
    Object? edited = null,
    Object? url = null,
  }) {
    return _then(_$CharacterImpl(
      name: null == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      height: null == height
          ? _value.height
          : height // ignore: cast_nullable_to_non_nullable
              as String,
      mass: null == mass
          ? _value.mass
          : mass // ignore: cast_nullable_to_non_nullable
              as String,
      hairColor: freezed == hairColor
          ? _value.hairColor
          : hairColor // ignore: cast_nullable_to_non_nullable
              as String?,
      skinColor: freezed == skinColor
          ? _value.skinColor
          : skinColor // ignore: cast_nullable_to_non_nullable
              as String?,
      eyeColor: freezed == eyeColor
          ? _value.eyeColor
          : eyeColor // ignore: cast_nullable_to_non_nullable
              as String?,
      birthYear: freezed == birthYear
          ? _value.birthYear
          : birthYear // ignore: cast_nullable_to_non_nullable
              as String?,
      gender: null == gender
          ? _value.gender
          : gender // ignore: cast_nullable_to_non_nullable
              as Gender,
      homeworld: null == homeworld
          ? _value.homeworld
          : homeworld // ignore: cast_nullable_to_non_nullable
              as String,
      films: null == films
          ? _value._films
          : films // ignore: cast_nullable_to_non_nullable
              as List<String>,
      species: null == species
          ? _value._species
          : species // ignore: cast_nullable_to_non_nullable
              as List<String>,
      vehicles: null == vehicles
          ? _value._vehicles
          : vehicles // ignore: cast_nullable_to_non_nullable
              as List<String>,
      starships: null == starships
          ? _value._starships
          : starships // ignore: cast_nullable_to_non_nullable
              as List<String>,
      created: null == created
          ? _value.created
          : created // ignore: cast_nullable_to_non_nullable
              as DateTime,
      edited: null == edited
          ? _value.edited
          : edited // ignore: cast_nullable_to_non_nullable
              as DateTime,
      url: null == url
          ? _value.url
          : url // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$CharacterImpl implements _Character {
  const _$CharacterImpl(
      {required this.name,
      required this.height,
      required this.mass,
      required this.hairColor,
      required this.skinColor,
      required this.eyeColor,
      required this.birthYear,
      @JsonKey(fromJson: genderFromJson, toJson: genderToJson) required this.gender,
      required this.homeworld,
      required final List<String> films,
      required final List<String> species,
      required final List<String> vehicles,
      required final List<String> starships,
      required this.created,
      required this.edited,
      required this.url})
      : _films = films,
        _species = species,
        _vehicles = vehicles,
        _starships = starships;

  factory _$CharacterImpl.fromJson(Map<String, dynamic> json) => _$$CharacterImplFromJson(json);

  @override
  final String name;
  @override
  final String height;
  @override
  final String mass;
  @override
  final String? hairColor;
  @override
  final String? skinColor;
  @override
  final String? eyeColor;
  @override
  final String? birthYear;
// ignore: invalid_annotation_target
  @override
  @JsonKey(fromJson: genderFromJson, toJson: genderToJson)
  final Gender gender;
  @override
  final String homeworld;
  final List<String> _films;
  @override
  List<String> get films {
    if (_films is EqualUnmodifiableListView) return _films;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_films);
  }

  final List<String> _species;
  @override
  List<String> get species {
    if (_species is EqualUnmodifiableListView) return _species;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_species);
  }

  final List<String> _vehicles;
  @override
  List<String> get vehicles {
    if (_vehicles is EqualUnmodifiableListView) return _vehicles;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_vehicles);
  }

  final List<String> _starships;
  @override
  List<String> get starships {
    if (_starships is EqualUnmodifiableListView) return _starships;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_starships);
  }

  @override
  final DateTime created;
  @override
  final DateTime edited;
  @override
  final String url;

  @override
  String toString() {
    return 'Character'; //(name: $name, height: $height, mass: $mass, hairColor: $hairColor, skinColor: $skinColor, eyeColor: $eyeColor, birthYear: $birthYear, gender: $gender, homeworld: $homeworld, films: $films, species: $species, vehicles: $vehicles, starships: $starships, created: $created, edited: $edited, url: $url)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$CharacterImpl &&
            (identical(other.name, name) || other.name == name) &&
            (identical(other.height, height) || other.height == height) &&
            (identical(other.mass, mass) || other.mass == mass) &&
            (identical(other.hairColor, hairColor) || other.hairColor == hairColor) &&
            (identical(other.skinColor, skinColor) || other.skinColor == skinColor) &&
            (identical(other.eyeColor, eyeColor) || other.eyeColor == eyeColor) &&
            (identical(other.birthYear, birthYear) || other.birthYear == birthYear) &&
            (identical(other.gender, gender) || other.gender == gender) &&
            (identical(other.homeworld, homeworld) || other.homeworld == homeworld) &&
            const DeepCollectionEquality().equals(other._films, _films) &&
            const DeepCollectionEquality().equals(other._species, _species) &&
            const DeepCollectionEquality().equals(other._vehicles, _vehicles) &&
            const DeepCollectionEquality().equals(other._starships, _starships) &&
            (identical(other.created, created) || other.created == created) &&
            (identical(other.edited, edited) || other.edited == edited) &&
            (identical(other.url, url) || other.url == url));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      name,
      height,
      mass,
      hairColor,
      skinColor,
      eyeColor,
      birthYear,
      gender,
      homeworld,
      const DeepCollectionEquality().hash(_films),
      const DeepCollectionEquality().hash(_species),
      const DeepCollectionEquality().hash(_vehicles),
      const DeepCollectionEquality().hash(_starships),
      created,
      edited,
      url);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$CharacterImplCopyWith<_$CharacterImpl> get copyWith =>
      __$$CharacterImplCopyWithImpl<_$CharacterImpl>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$CharacterImplToJson(
      this,
    );
  }
}

abstract class _Character implements Character {
  const factory _Character(
      {required final String name,
      required final String height,
      required final String mass,
      required final String? hairColor,
      required final String? skinColor,
      required final String? eyeColor,
      required final String? birthYear,
      @JsonKey(fromJson: genderFromJson, toJson: genderToJson) required final Gender gender,
      required final String homeworld,
      required final List<String> films,
      required final List<String> species,
      required final List<String> vehicles,
      required final List<String> starships,
      required final DateTime created,
      required final DateTime edited,
      required final String url}) = _$CharacterImpl;

  factory _Character.fromJson(Map<String, dynamic> json) = _$CharacterImpl.fromJson;

  @override
  String get name;
  @override
  String get height;
  @override
  String get mass;
  @override
  String? get hairColor;
  @override
  String? get skinColor;
  @override
  String? get eyeColor;
  @override
  String? get birthYear;
  @override // ignore: invalid_annotation_target
  @JsonKey(fromJson: genderFromJson, toJson: genderToJson)
  Gender get gender;
  @override
  String get homeworld;
  @override
  List<String> get films;
  @override
  List<String> get species;
  @override
  List<String> get vehicles;
  @override
  List<String> get starships;
  @override
  DateTime get created;
  @override
  DateTime get edited;
  @override
  String get url;
  @override
  @JsonKey(ignore: true)
  _$$CharacterImplCopyWith<_$CharacterImpl> get copyWith => throw _privateConstructorUsedError;
}
