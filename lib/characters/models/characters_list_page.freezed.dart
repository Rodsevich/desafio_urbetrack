// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'characters_list_page.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

CharactersListPage _$CharactersListPageFromJson(Map<String, dynamic> json) {
  return _CharactersListPage.fromJson(json);
}

/// @nodoc
mixin _$CharactersListPage {
  int get count => throw _privateConstructorUsedError;
  String? get next => throw _privateConstructorUsedError;
  String? get previous => throw _privateConstructorUsedError;
  List<Character> get results => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $CharactersListPageCopyWith<CharactersListPage> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $CharactersListPageCopyWith<$Res> {
  factory $CharactersListPageCopyWith(
          CharactersListPage value, $Res Function(CharactersListPage) then) =
      _$CharactersListPageCopyWithImpl<$Res, CharactersListPage>;
  @useResult
  $Res call(
      {int count, String? next, String? previous, List<Character> results});
}

/// @nodoc
class _$CharactersListPageCopyWithImpl<$Res, $Val extends CharactersListPage>
    implements $CharactersListPageCopyWith<$Res> {
  _$CharactersListPageCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? count = null,
    Object? next = freezed,
    Object? previous = freezed,
    Object? results = null,
  }) {
    return _then(_value.copyWith(
      count: null == count
          ? _value.count
          : count // ignore: cast_nullable_to_non_nullable
              as int,
      next: freezed == next
          ? _value.next
          : next // ignore: cast_nullable_to_non_nullable
              as String?,
      previous: freezed == previous
          ? _value.previous
          : previous // ignore: cast_nullable_to_non_nullable
              as String?,
      results: null == results
          ? _value.results
          : results // ignore: cast_nullable_to_non_nullable
              as List<Character>,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$CharactersListPageImplCopyWith<$Res>
    implements $CharactersListPageCopyWith<$Res> {
  factory _$$CharactersListPageImplCopyWith(_$CharactersListPageImpl value,
          $Res Function(_$CharactersListPageImpl) then) =
      __$$CharactersListPageImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {int count, String? next, String? previous, List<Character> results});
}

/// @nodoc
class __$$CharactersListPageImplCopyWithImpl<$Res>
    extends _$CharactersListPageCopyWithImpl<$Res, _$CharactersListPageImpl>
    implements _$$CharactersListPageImplCopyWith<$Res> {
  __$$CharactersListPageImplCopyWithImpl(_$CharactersListPageImpl _value,
      $Res Function(_$CharactersListPageImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? count = null,
    Object? next = freezed,
    Object? previous = freezed,
    Object? results = null,
  }) {
    return _then(_$CharactersListPageImpl(
      count: null == count
          ? _value.count
          : count // ignore: cast_nullable_to_non_nullable
              as int,
      next: freezed == next
          ? _value.next
          : next // ignore: cast_nullable_to_non_nullable
              as String?,
      previous: freezed == previous
          ? _value.previous
          : previous // ignore: cast_nullable_to_non_nullable
              as String?,
      results: null == results
          ? _value._results
          : results // ignore: cast_nullable_to_non_nullable
              as List<Character>,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$CharactersListPageImpl implements _CharactersListPage {
  const _$CharactersListPageImpl(
      {required this.count,
      required this.next,
      required this.previous,
      required final List<Character> results})
      : _results = results;

  factory _$CharactersListPageImpl.fromJson(Map<String, dynamic> json) =>
      _$$CharactersListPageImplFromJson(json);

  @override
  final int count;
  @override
  final String? next;
  @override
  final String? previous;
  final List<Character> _results;
  @override
  List<Character> get results {
    if (_results is EqualUnmodifiableListView) return _results;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_results);
  }

  @override
  String toString() {
    return 'CharactersListPage(count: $count, next: $next, previous: $previous, results: $results)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$CharactersListPageImpl &&
            (identical(other.count, count) || other.count == count) &&
            (identical(other.next, next) || other.next == next) &&
            (identical(other.previous, previous) ||
                other.previous == previous) &&
            const DeepCollectionEquality().equals(other._results, _results));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, count, next, previous,
      const DeepCollectionEquality().hash(_results));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$CharactersListPageImplCopyWith<_$CharactersListPageImpl> get copyWith =>
      __$$CharactersListPageImplCopyWithImpl<_$CharactersListPageImpl>(
          this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$CharactersListPageImplToJson(
      this,
    );
  }
}

abstract class _CharactersListPage implements CharactersListPage {
  const factory _CharactersListPage(
      {required final int count,
      required final String? next,
      required final String? previous,
      required final List<Character> results}) = _$CharactersListPageImpl;

  factory _CharactersListPage.fromJson(Map<String, dynamic> json) =
      _$CharactersListPageImpl.fromJson;

  @override
  int get count;
  @override
  String? get next;
  @override
  String? get previous;
  @override
  List<Character> get results;
  @override
  @JsonKey(ignore: true)
  _$$CharactersListPageImplCopyWith<_$CharactersListPageImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
