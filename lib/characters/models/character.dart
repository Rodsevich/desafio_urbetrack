import 'package:freezed_annotation/freezed_annotation.dart';

part 'character.freezed.dart';
part 'character.g.dart';

@freezed
class Character with _$Character {
  const factory Character({
    required String name,
    required String height,
    required String mass,
    required String? hairColor,
    required String? skinColor,
    required String? eyeColor,
    required String? birthYear,
    // ignore: invalid_annotation_target
    @JsonKey(fromJson: genderFromJson, toJson: genderToJson) required Gender gender,
    required String homeworld,
    required List<String> films,
    required List<String> species,
    required List<String> vehicles,
    required List<String> starships,
    required DateTime created,
    required DateTime edited,
    required String url,
  }) = _Character;

  factory Character.fromJson(Map<String, dynamic> json) => _$CharacterFromJson(json);
}

Map<String, dynamic> genderToJson(Gender gender) => {'gender': gender.name};

Gender genderFromJson(jsonValue) => switch (jsonValue) {
      'female' => Gender.female,
      'male' => Gender.male,
      'hermaphrodite' => Gender.hermaphrodite,
      _ => Gender.notAny,
    };

enum Gender { female, hermaphrodite, male, notAny }
