// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'characters_list_page.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$CharactersListPageImpl _$$CharactersListPageImplFromJson(
        Map<String, dynamic> json) =>
    _$CharactersListPageImpl(
      count: json['count'] as int,
      next: json['next'] as String?,
      previous: json['previous'] as String?,
      results: (json['results'] as List<dynamic>)
          .map((e) => Character.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$$CharactersListPageImplToJson(
        _$CharactersListPageImpl instance) =>
    <String, dynamic>{
      'count': instance.count,
      'next': instance.next,
      'previous': instance.previous,
      'results': instance.results,
    };
