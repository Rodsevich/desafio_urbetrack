import 'package:dartz/dartz.dart';
import 'package:desafio_urbetrack/characters/models/characters_list_page.dart';
import 'package:desafio_urbetrack/net/base_repos/repo_starwars_base.dart';
import 'package:desafio_urbetrack/net/errors.dart';
import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';

import '../models/character.dart';

class RepoCharacters extends BaseRepoStarwars {
  RepoCharacters() : super('/people');

  Future<Either<NetErrors, CharactersListPage>> getCharacters({int page = 1}) async {
    try {
      final response = await client.get<Map<String, dynamic>>(resource, queryParameters: {'page': page});
      if (response.statusCode == 200) {
        if (response.data != null) {
          return Right(CharactersListPage.fromJson(response.data!));
        } else {
          return const Left(NetErrors.emptyResponseData);
        }
      } else {
        return Left(
          NetErrors.values.singleWhere(
            (error) => error.httpStatusCode == response.statusCode,
            orElse: () => NetErrors.unknownError,
          ),
        );
      }
    } on DioException catch (e) {
      if (e.response == null) {
        return const Left(NetErrors.noConnection);
      } else {
        return Left(
          NetErrors.values.singleWhere(
            (error) => error.httpStatusCode == e.response!.statusCode,
            orElse: () => NetErrors.unknownError,
          ),
        );
      }
    } catch (e) {
      if (kDebugMode) rethrow;
      return const Left(NetErrors.unknownError);
    }
  }
}
