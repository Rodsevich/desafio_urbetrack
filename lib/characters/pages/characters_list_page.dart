import 'package:auto_route/auto_route.dart';
import 'package:desafio_urbetrack/app/blocs/settings/settings_bloc.dart';
import 'package:desafio_urbetrack/app/dialogs/base/alert_dialog.dart';
import 'package:desafio_urbetrack/app/dialogs/sin_internet.dart';
import 'package:desafio_urbetrack/app/routes.dart';
import 'package:desafio_urbetrack/characters/bloc/characters_bloc.dart';
import 'package:desafio_urbetrack/characters/bloc/characters_bloc_states.dart';
import 'package:desafio_urbetrack/characters/gender_widget.dart';
import 'package:desafio_urbetrack/characters/repos/characters_repo.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:desafio_urbetrack/l10n/l10n.dart';

import '../../app/blocs/settings/settings_bloc_state.dart';
import '../../app/components/urbe_scaffold.dart';
import '../../net/errors.dart';
import '../bloc/characters_bloc_events.dart';

@RoutePage()
class PageCharactersList extends StatelessWidget {
  const PageCharactersList({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => BlocCharacters(context.read<BlocSettings>(), context.read<RepoCharacters>())
        ..add(const BlocCharactersEventinitialize()),
      child: const ViewCharactersList(),
    );
  }
}

class ViewCharactersList extends StatelessWidget {
  const ViewCharactersList({super.key});

  @override
  Widget build(BuildContext context) {
    final l10n = context.l10n;
    final blocCharacters = context.read<BlocCharacters>();
    return UrbeScaffold(
      title: l10n.pageCharactersListAppBarTitle,
      body: BlocConsumer<BlocCharacters, BlocCharactersState>(
        builder: (context, state) {
          if (state is BlocCharactersStateInitializing) {
            return const Center(child: CircularProgressIndicator());
          } else if (state is BlocCharactersStateListLoaded ||
              state is BlocCharactersStateLoading ||
              state is BlocCharactersStateDetailsLoaded) {
            return Column(
              children: [
                Expanded(
                  child: ListView.builder(
                    itemCount: state.characters.length,
                    itemBuilder: (context, i) => ListTile(
                      title: Row(
                          children: [GenderWidget(gender: state.characters[i].gender), Text(state.characters[i].name)]),
                      leading: Hero(
                          tag: state.characters[i].url,
                          child: CircleAvatar(backgroundImage: Image.asset('assets/images/dash_vader.png').image)),
                      onTap: () => blocCharacters.add(BlocCharactersEventCharacterChosen(state.characters[i])),
                    ),
                  ),
                ),
                BlocBuilder<BlocSettings, BlocSettingsState>(
                  builder: (context, settingsState) {
                    return Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        ElevatedButton(
                            onPressed: (state.canGoPreviousPage(settingsState.offlineMode))
                                ? () => blocCharacters.add(BlocCharactersEventPreviousPage())
                                : null,
                            child: Text('Anterior')),
                        Column(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Text('Página ${state.currentPageNumber} de ${state.totalPages}'),
                            if (state is BlocCharactersStateLoading)
                              const SizedBox(
                                width: 150,
                                height: 12,
                                child: LinearProgressIndicator(
                                  minHeight: 15,
                                ),
                              )
                          ],
                        ),
                        ElevatedButton(
                            onPressed: (state.canGoNextPage(settingsState.offlineMode))
                                ? () => blocCharacters.add(BlocCharactersEventNextPage())
                                : null,
                            child: Text('Siguiente')),
                      ],
                    );
                  },
                )
              ],
            );
          } else {
            return Center(child: Text('Unexpected state: $state. Please report this bug.'));
          }
        },
        listenWhen: (previous, current) =>
            current is BlocCharactersStateDetailsLoaded || current is BlocCharactersStateError,
        listener: (BuildContext context, BlocCharactersState state) {
          if (state is BlocCharactersStateDetailsLoaded) {
            context.router.push(
              RouteCharacterDetail(character: state.character, blocCharacters: blocCharacters),
            );
          } else if (state is BlocCharactersStateError) {
            if (state.error is NetErrors && state.error == NetErrors.noConnection) {
              showDialog<Never>(context: context, builder: (context) => UrbeDialogSinInternet());
            } else {
              ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text(state.error.toString())));
            }
          } else {
            throw UnsupportedError('error imposible. reportalo xfa');
          }
        },
      ),
    );
  }
}
