import 'package:auto_route/auto_route.dart';
import 'package:desafio_urbetrack/characters/bloc/characters_bloc.dart';
import 'package:desafio_urbetrack/characters/bloc/characters_bloc_events.dart';
import 'package:desafio_urbetrack/characters/gender_widget.dart';
import 'package:flutter/material.dart';

import 'package:desafio_urbetrack/app/components/urbe_scaffold.dart';
import 'package:desafio_urbetrack/characters/models/character.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

@RoutePage()
class PageCharacterDetail extends StatelessWidget {
  const PageCharacterDetail({required this.character, super.key, required this.blocCharacters});

  final Character character;
  final BlocCharacters blocCharacters;

  @override
  Widget build(BuildContext context) {
    return ViewCharacterDetail(
      character: character,
      blocCharacters: blocCharacters,
    );
  }
}

class ViewCharacterDetail extends StatelessWidget {
  const ViewCharacterDetail({super.key, required this.character, required this.blocCharacters});

  final Character character;
  final BlocCharacters blocCharacters;

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        blocCharacters.add(const BlocCharactersEventDetailsBack());
        return Future.value(true);
      },
      child: UrbeScaffold(
        title: character.name,
        body: Padding(
          padding: const EdgeInsets.all(18.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Hero(tag: character.url, child: Image.asset('assets/images/dash_vader.png')),
              Text('Name: ${character.name}'),
              Row(children: [
                const Text('Gender:'),
                GenderWidget(gender: character.gender),
                Text(character.gender.name),
              ]),
              Text('Birth year: ${character.birthYear}'),
              Text('Species: ${character.species}'),
              Text('Eye color: ${character.eyeColor}'),
              Text('Hair color: ${character.hairColor}'),
              Text('Skin color: ${character.skinColor}'),
              Text('Height: ${character.height}'),
              Text('Mass: ${character.mass}'),
            ],
          ),
        ),
      ),
    );
  }
}
