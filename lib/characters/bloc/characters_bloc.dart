import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:desafio_urbetrack/app/blocs/settings/settings_bloc.dart';
import 'package:desafio_urbetrack/characters/repos/characters_repo.dart';
import 'package:desafio_urbetrack/net/errors.dart';

import 'characters_bloc_events.dart';
import 'characters_bloc_states.dart';

class BlocCharacters extends Bloc<BlocCharactersEvent, BlocCharactersState> {
  final BlocSettings settingsBloc;
  final RepoCharacters repoCharacters;

  static const int pageCountlimit = 10;

  BlocCharacters(this.settingsBloc, this.repoCharacters) : super(const BlocCharactersStateInitializing()) {
    on<BlocCharactersEventPreviousPage>(_onPreviousPageTapped);
    on<BlocCharactersEventNextPage>(_onNextPageTapped);
    on<BlocCharactersEventCharacterChosen>(_onCharacterChosen);
    on<BlocCharactersEventDetailsBack>(_onDetailsBack);
    on<BlocCharactersEventinitialize>(_onInitialize);
  }

  Future<FutureOr<void>> _onInitialize(BlocCharactersEventinitialize event, Emitter<BlocCharactersState> emit) async {
    emit(const BlocCharactersStateInitializing());
    if (settingsBloc.state.offlineMode) {
      emit(BlocCharactersStateError.from(NetErrors.noConnection, state));
    } else {
      (await repoCharacters.getCharacters()).fold((error) => emit(BlocCharactersStateError.from(error, state)),
          (charspage) => emit(BlocCharactersStateListLoaded.fromPage(charspage, 1)));
    }
  }

  FutureOr<void> _onCharacterChosen(BlocCharactersEventCharacterChosen event, Emitter<BlocCharactersState> emit) {
    final newState = BlocCharactersStateDetailsLoaded.from(event.character, state);
    emit(newState);
  }

  Future<FutureOr<void>> _onNextPageTapped(BlocCharactersEventNextPage event, Emitter<BlocCharactersState> emit) async {
    if (settingsBloc.state.offlineMode) {
      emit(BlocCharactersStateError.from(NetErrors.noConnection, state));
    } else if (state.canGoNextPage(settingsBloc.state.offlineMode)) {
      emit(BlocCharactersStateLoading.from(state));
      final newPageNumber = state.currentPageNumber + 1;
      (await repoCharacters.getCharacters(page: newPageNumber)).fold(
          (error) => emit(BlocCharactersStateError.from(error, state)),
          (charspage) => emit(BlocCharactersStateListLoaded.fromPage(charspage, newPageNumber)));
    }
  }

  Future<FutureOr<void>> _onPreviousPageTapped(
      BlocCharactersEventPreviousPage event, Emitter<BlocCharactersState> emit) async {
    if (settingsBloc.state.offlineMode) {
      emit(BlocCharactersStateError.from(NetErrors.noConnection, state));
    } else if (state.canGoPreviousPage(settingsBloc.state.offlineMode)) {
      emit(BlocCharactersStateLoading.from(state));
      final newPageNumber = state.currentPageNumber - 1;
      (await repoCharacters.getCharacters(page: newPageNumber)).fold(
          (error) => emit(BlocCharactersStateError.from(error, state)),
          (charspage) => emit(BlocCharactersStateListLoaded.fromPage(charspage, newPageNumber)));
    }
  }

  FutureOr<void> _onDetailsBack(BlocCharactersEventDetailsBack event, Emitter<BlocCharactersState> emit) {
    emit(BlocCharactersStateListLoaded.from(state));
  }
}
