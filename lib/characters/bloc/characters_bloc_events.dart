import 'package:desafio_urbetrack/characters/models/character.dart';
import 'package:equatable/equatable.dart';

abstract class BlocCharactersEvent extends Equatable {
  const BlocCharactersEvent();

  @override
  List<Object> get props => [];
}

class BlocCharactersEventCharacterChosen extends BlocCharactersEvent {
  const BlocCharactersEventCharacterChosen(this.character);

  final Character character;

  @override
  List<Object> get props => [character];
}

class BlocCharactersEventinitialize extends BlocCharactersEvent {
  const BlocCharactersEventinitialize();
}

class BlocCharactersEventPageChange extends BlocCharactersEvent {
  const BlocCharactersEventPageChange();
}

class BlocCharactersEventNextPage extends BlocCharactersEvent {
  const BlocCharactersEventNextPage();
}

class BlocCharactersEventPreviousPage extends BlocCharactersEvent {
  const BlocCharactersEventPreviousPage();
}

class BlocCharactersEventDetailsBack extends BlocCharactersEvent {
  const BlocCharactersEventDetailsBack();
}
