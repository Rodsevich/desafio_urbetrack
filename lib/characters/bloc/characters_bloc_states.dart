import 'package:desafio_urbetrack/characters/bloc/characters_bloc.dart';
import 'package:desafio_urbetrack/characters/models/characters_list_page.dart';
import 'package:equatable/equatable.dart';

import '../models/character.dart';

sealed class BlocCharactersState extends Equatable {
  const BlocCharactersState({
    this.characters = const [],
    this.totalCharactersAmount = 0,
    this.currentPageNumber = 1,
  });

  BlocCharactersState.from(BlocCharactersState other,
      {List<Character>? characters, int? totalCharactersAmount, int? currentPageNumber})
      : this(
          characters: characters ?? [...other.characters],
          totalCharactersAmount: totalCharactersAmount ?? other.totalCharactersAmount,
          currentPageNumber: currentPageNumber ?? other.currentPageNumber,
        );

  final int currentPageNumber;
  final List<Character> characters;
  final int totalCharactersAmount;

  int get totalPages => (totalCharactersAmount / BlocCharacters.pageCountlimit).ceil();

  bool canGoNextPage(bool offlineMode) => (currentPageNumber != totalPages) && !offlineMode;

  bool canGoPreviousPage(bool offlineMode) => (currentPageNumber > 1) && !offlineMode;

  @override
  List<Object> get props => [currentPageNumber, characters, totalCharactersAmount];
}

class BlocCharactersStateError extends BlocCharactersState {
  BlocCharactersStateError.from(
    this.error,
    BlocCharactersState other,
  ) : super.from(other);

  final Object error;

  @override
  List<Object> get props => [...super.props, error];
}

class BlocCharactersStateInitializing extends BlocCharactersState {
  const BlocCharactersStateInitializing();
}

class BlocCharactersStateLoading extends BlocCharactersState {
  BlocCharactersStateLoading.from(BlocCharactersState other) : super.from(other);
}

class BlocCharactersStateListLoaded extends BlocCharactersState {
  BlocCharactersStateListLoaded.from(BlocCharactersState other) : super.from(other);

  BlocCharactersStateListLoaded.fromPage(CharactersListPage retrievedPage, int currentPageNumber)
      : super(
          characters: retrievedPage.results,
          totalCharactersAmount: retrievedPage.count,
          currentPageNumber: currentPageNumber,
        );
}

class BlocCharactersStateDetailsLoaded extends BlocCharactersState {
  BlocCharactersStateDetailsLoaded.from(
    this.character,
    BlocCharactersState other,
  ) : super.from(other);

  final Character character;

  @override
  List<Object> get props => [...super.props, character];
}
