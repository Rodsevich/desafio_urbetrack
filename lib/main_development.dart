import 'package:desafio_urbetrack/app/app.dart';
import 'package:desafio_urbetrack/bootstrap.dart';

void main() {
  bootstrap(() => const App());
}
