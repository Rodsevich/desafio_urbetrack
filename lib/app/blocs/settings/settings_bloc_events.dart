import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

sealed class BlocSettingsEvent extends Equatable {
  const BlocSettingsEvent();
}

class BlocSettingsEventThemeModeChanged extends BlocSettingsEvent {
  const BlocSettingsEventThemeModeChanged({required this.themeMode});
  final ThemeMode themeMode;

  @override
  List<Object> get props => [themeMode];
}

class BlocSettingsEventLocaleChanged extends BlocSettingsEvent {
  const BlocSettingsEventLocaleChanged({required this.locale});
  final Locale locale;

  @override
  List<Object> get props => [locale];
}

class BlocSettingsEventOfflineModeChanged extends BlocSettingsEvent {
  const BlocSettingsEventOfflineModeChanged({required this.offlineMode});
  final bool offlineMode;

  @override
  List<Object> get props => [offlineMode];
}
