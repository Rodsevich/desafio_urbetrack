// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'settings_bloc_state.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

BlocSettingsState _$BlocSettingsStateFromJson(Map<String, dynamic> json) {
  return _BlocSettingsState.fromJson(json);
}

/// @nodoc
mixin _$BlocSettingsState {
  ThemeMode get themeMode => throw _privateConstructorUsedError;
  @LocaleSerialization()
  Locale get locale => throw _privateConstructorUsedError;
  bool get offlineMode => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $BlocSettingsStateCopyWith<BlocSettingsState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $BlocSettingsStateCopyWith<$Res> {
  factory $BlocSettingsStateCopyWith(
          BlocSettingsState value, $Res Function(BlocSettingsState) then) =
      _$BlocSettingsStateCopyWithImpl<$Res, BlocSettingsState>;
  @useResult
  $Res call(
      {ThemeMode themeMode,
      @LocaleSerialization() Locale locale,
      bool offlineMode});
}

/// @nodoc
class _$BlocSettingsStateCopyWithImpl<$Res, $Val extends BlocSettingsState>
    implements $BlocSettingsStateCopyWith<$Res> {
  _$BlocSettingsStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? themeMode = null,
    Object? locale = null,
    Object? offlineMode = null,
  }) {
    return _then(_value.copyWith(
      themeMode: null == themeMode
          ? _value.themeMode
          : themeMode // ignore: cast_nullable_to_non_nullable
              as ThemeMode,
      locale: null == locale
          ? _value.locale
          : locale // ignore: cast_nullable_to_non_nullable
              as Locale,
      offlineMode: null == offlineMode
          ? _value.offlineMode
          : offlineMode // ignore: cast_nullable_to_non_nullable
              as bool,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$BlocSettingsStateImplCopyWith<$Res>
    implements $BlocSettingsStateCopyWith<$Res> {
  factory _$$BlocSettingsStateImplCopyWith(_$BlocSettingsStateImpl value,
          $Res Function(_$BlocSettingsStateImpl) then) =
      __$$BlocSettingsStateImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {ThemeMode themeMode,
      @LocaleSerialization() Locale locale,
      bool offlineMode});
}

/// @nodoc
class __$$BlocSettingsStateImplCopyWithImpl<$Res>
    extends _$BlocSettingsStateCopyWithImpl<$Res, _$BlocSettingsStateImpl>
    implements _$$BlocSettingsStateImplCopyWith<$Res> {
  __$$BlocSettingsStateImplCopyWithImpl(_$BlocSettingsStateImpl _value,
      $Res Function(_$BlocSettingsStateImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? themeMode = null,
    Object? locale = null,
    Object? offlineMode = null,
  }) {
    return _then(_$BlocSettingsStateImpl(
      themeMode: null == themeMode
          ? _value.themeMode
          : themeMode // ignore: cast_nullable_to_non_nullable
              as ThemeMode,
      locale: null == locale
          ? _value.locale
          : locale // ignore: cast_nullable_to_non_nullable
              as Locale,
      offlineMode: null == offlineMode
          ? _value.offlineMode
          : offlineMode // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$BlocSettingsStateImpl implements _BlocSettingsState {
  const _$BlocSettingsStateImpl(
      {this.themeMode = ThemeMode.system,
      @LocaleSerialization() this.locale = const Locale('es', 'AR'),
      this.offlineMode = false});

  factory _$BlocSettingsStateImpl.fromJson(Map<String, dynamic> json) =>
      _$$BlocSettingsStateImplFromJson(json);

  @override
  @JsonKey()
  final ThemeMode themeMode;
  @override
  @JsonKey()
  @LocaleSerialization()
  final Locale locale;
  @override
  @JsonKey()
  final bool offlineMode;

  @override
  String toString() {
    return 'BlocSettingsState(themeMode: $themeMode, locale: $locale, offlineMode: $offlineMode)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$BlocSettingsStateImpl &&
            (identical(other.themeMode, themeMode) ||
                other.themeMode == themeMode) &&
            (identical(other.locale, locale) || other.locale == locale) &&
            (identical(other.offlineMode, offlineMode) ||
                other.offlineMode == offlineMode));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, themeMode, locale, offlineMode);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$BlocSettingsStateImplCopyWith<_$BlocSettingsStateImpl> get copyWith =>
      __$$BlocSettingsStateImplCopyWithImpl<_$BlocSettingsStateImpl>(
          this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$BlocSettingsStateImplToJson(
      this,
    );
  }
}

abstract class _BlocSettingsState implements BlocSettingsState {
  const factory _BlocSettingsState(
      {final ThemeMode themeMode,
      @LocaleSerialization() final Locale locale,
      final bool offlineMode}) = _$BlocSettingsStateImpl;

  factory _BlocSettingsState.fromJson(Map<String, dynamic> json) =
      _$BlocSettingsStateImpl.fromJson;

  @override
  ThemeMode get themeMode;
  @override
  @LocaleSerialization()
  Locale get locale;
  @override
  bool get offlineMode;
  @override
  @JsonKey(ignore: true)
  _$$BlocSettingsStateImplCopyWith<_$BlocSettingsStateImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
