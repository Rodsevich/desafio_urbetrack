import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:hydrated_bloc/hydrated_bloc.dart';

import 'settings_bloc_events.dart';
import 'settings_bloc_state.dart';

class BlocSettings extends HydratedBloc<BlocSettingsEvent, BlocSettingsState> {
  BlocSettings() : super(const BlocSettingsState()) {
    on<BlocSettingsEventThemeModeChanged>(_onThemeModechanged);
    on<BlocSettingsEventLocaleChanged>(_onLocaleChanged);
    on<BlocSettingsEventOfflineModeChanged>(_onOfflineModeChanged);
  }

  FutureOr<void> _onOfflineModeChanged(BlocSettingsEventOfflineModeChanged event, Emitter<BlocSettingsState> emit) {
    emit(state.copyWith(offlineMode: event.offlineMode));
  }

  FutureOr<void> _onThemeModechanged(BlocSettingsEventThemeModeChanged event, Emitter<BlocSettingsState> emit) {
    emit(state.copyWith(themeMode: event.themeMode));
  }

  FutureOr<void> _onLocaleChanged(BlocSettingsEventLocaleChanged event, Emitter<BlocSettingsState> emit) {
    emit(state.copyWith(locale: event.locale));
  }

  @override
  BlocSettingsState? fromJson(Map<String, dynamic> json) {
    return BlocSettingsState.fromJson(json);
  }

  @override
  Map<String, dynamic>? toJson(BlocSettingsState state) {
    return state.toJson();
  }
}
