// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'settings_bloc_state.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$BlocSettingsStateImpl _$$BlocSettingsStateImplFromJson(
        Map<String, dynamic> json) =>
    _$BlocSettingsStateImpl(
      themeMode: $enumDecodeNullable(_$ThemeModeEnumMap, json['themeMode']) ??
          ThemeMode.system,
      locale: json['locale'] == null
          ? const Locale('es', 'AR')
          : const LocaleSerialization()
              .fromJson(json['locale'] as Map<String, dynamic>),
      offlineMode: json['offlineMode'] as bool? ?? false,
    );

Map<String, dynamic> _$$BlocSettingsStateImplToJson(
        _$BlocSettingsStateImpl instance) =>
    <String, dynamic>{
      'themeMode': _$ThemeModeEnumMap[instance.themeMode]!,
      'locale': const LocaleSerialization().toJson(instance.locale),
      'offlineMode': instance.offlineMode,
    };

const _$ThemeModeEnumMap = {
  ThemeMode.system: 'system',
  ThemeMode.light: 'light',
  ThemeMode.dark: 'dark',
};
