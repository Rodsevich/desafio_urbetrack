import 'package:flutter/material.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'settings_bloc_state.freezed.dart';
part 'settings_bloc_state.g.dart';

@freezed
class BlocSettingsState with _$BlocSettingsState {
  const factory BlocSettingsState({
    @Default(ThemeMode.system) ThemeMode themeMode,
    @LocaleSerialization() @Default(Locale('es', 'AR')) Locale locale,
    @Default(false) bool offlineMode,
  }) = _BlocSettingsState;

  factory BlocSettingsState.fromJson(Map<String, dynamic> json) => _$BlocSettingsStateFromJson(json);
}

class LocaleSerialization implements JsonConverter<Locale, Map<String, dynamic>> {
  const LocaleSerialization();

  Map<String, dynamic> toJson(Locale locale) => {
        'languageCode': locale.languageCode,
        'countryCode': locale.countryCode,
      };

  Locale fromJson(Map<String, dynamic> json) => Locale(
        json['languageCode'] as String? ?? 'es',
        json['countryCode'] as String? ?? 'AR',
      );
}
