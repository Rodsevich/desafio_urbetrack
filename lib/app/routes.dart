import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';

import '../characters/bloc/characters_bloc.dart';
import '../characters/models/character.dart';
import '../characters/pages/characters_list_page.dart';
import '../characters/pages/character_details_page.dart';
part 'routes.gr.dart';

@AutoRouterConfig()
class AppRouter extends _$AppRouter {
  @override
  List<AutoRoute> get routes => [
        CustomRoute(page: RouteCharactersList.page, initial: true, durationInMilliseconds: 450),
        CustomRoute(
            page: RouteCharacterDetail.page,
            transitionsBuilder: TransitionsBuilders.slideRightWithFade,
            durationInMilliseconds: 700),
      ];
}
