// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouterGenerator
// **************************************************************************

// ignore_for_file: type=lint
// coverage:ignore-file

part of 'routes.dart';

abstract class _$AppRouter extends RootStackRouter {
  // ignore: unused_element
  _$AppRouter({super.navigatorKey});

  @override
  final Map<String, PageFactory> pagesMap = {
    RouteCharacterDetail.name: (routeData) {
      final args = routeData.argsAs<RouteCharacterDetailArgs>();
      return AutoRoutePage<dynamic>(
        routeData: routeData,
        child: PageCharacterDetail(
          character: args.character,
          key: args.key,
          blocCharacters: args.blocCharacters,
        ),
      );
    },
    RouteCharactersList.name: (routeData) {
      return AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const PageCharactersList(),
      );
    },
  };
}

/// generated route for
/// [PageCharacterDetail]
class RouteCharacterDetail extends PageRouteInfo<RouteCharacterDetailArgs> {
  RouteCharacterDetail({
    required Character character,
    Key? key,
    required BlocCharacters blocCharacters,
    List<PageRouteInfo>? children,
  }) : super(
          RouteCharacterDetail.name,
          args: RouteCharacterDetailArgs(
            character: character,
            key: key,
            blocCharacters: blocCharacters,
          ),
          initialChildren: children,
        );

  static const String name = 'RouteCharacterDetail';

  static const PageInfo<RouteCharacterDetailArgs> page =
      PageInfo<RouteCharacterDetailArgs>(name);
}

class RouteCharacterDetailArgs {
  const RouteCharacterDetailArgs({
    required this.character,
    this.key,
    required this.blocCharacters,
  });

  final Character character;

  final Key? key;

  final BlocCharacters blocCharacters;

  @override
  String toString() {
    return 'RouteCharacterDetailArgs{character: $character, key: $key, blocCharacters: $blocCharacters}';
  }
}

/// generated route for
/// [PageCharactersList]
class RouteCharactersList extends PageRouteInfo<void> {
  const RouteCharactersList({List<PageRouteInfo>? children})
      : super(
          RouteCharactersList.name,
          initialChildren: children,
        );

  static const String name = 'RouteCharactersList';

  static const PageInfo<void> page = PageInfo<void>(name);
}
