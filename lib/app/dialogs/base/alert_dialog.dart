import 'package:flutter/material.dart';

class UrbeAlertDialog extends StatelessWidget {
  const UrbeAlertDialog({
    required this.title,
    required this.message,
    required this.buttonText,
    required this.onPressed,
    super.key,
  });

  final String title;
  final String message;
  final String buttonText;
  final VoidCallback onPressed;

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text(title),
      content: Text(message),
      actions: [
        TextButton(
          onPressed: onPressed,
          child: Text(buttonText),
        ),
      ],
    );
  }
}
