import 'package:auto_route/auto_route.dart';
import 'package:desafio_urbetrack/app/dialogs/base/alert_dialog.dart';
import 'package:flutter/material.dart';

class UrbeDialogSinInternet extends StatelessWidget {
  const UrbeDialogSinInternet({super.key});

  @override
  Widget build(BuildContext context) {
    return UrbeAlertDialog(
        title: 'Sin internet',
        message: 'Estás undercover, andá al drawer y activá inernet, '
            'pero no te olvides de volver a desactivarlo para no levantar la perdiz',
        buttonText: 'Dale, papá',
        onPressed: () => context.router.pop());
  }
}
