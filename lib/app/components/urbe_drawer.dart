import 'package:auto_route/auto_route.dart';
import 'package:desafio_urbetrack/app/blocs/settings/settings_bloc_events.dart';
import 'package:desafio_urbetrack/app/dialogs/base/confirmation_dialog.dart';
import 'package:desafio_urbetrack/l10n/l10n.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hydrated_bloc/hydrated_bloc.dart';

import '../blocs/settings/settings_bloc.dart';
import '../blocs/settings/settings_bloc_state.dart';

class UrbeDrawer extends StatelessWidget {
  const UrbeDrawer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final blocSettings = context.read<BlocSettings>();
    final l10n = context.l10n;
    return Drawer(
      child: BlocBuilder<BlocSettings, BlocSettingsState>(
        builder: (context, settingsState) {
          final isCurrentModeDark = Theme.of(context).brightness == Brightness.dark;
          return ListView(
            padding: EdgeInsets.zero,
            children: [
              DrawerHeader(
                child: Text('UrbeTrack'),
                decoration: BoxDecoration(
                  color: Theme.of(context).primaryColor,
                ),
              ),
              ListTile(
                title: Text('Offline mode'),
                trailing: Switch(
                  value: settingsState.offlineMode,
                  onChanged: (value) => blocSettings.add(
                    BlocSettingsEventOfflineModeChanged(
                      offlineMode: !settingsState.offlineMode,
                    ),
                  ),
                ),
              ),
              ListTile(
                title: Text(l10n.drawerDarkModeTitle),
                trailing: Switch(
                  value: isCurrentModeDark,
                  onChanged: (value) => blocSettings.add(
                    BlocSettingsEventThemeModeChanged(
                      themeMode: isCurrentModeDark ? ThemeMode.light : ThemeMode.dark,
                    ),
                  ),
                ),
              ),
              ListTile(
                title: Text('Language'),
                trailing: DropdownButton<Locale>(
                  value: Localizations.localeOf(context),
                  onChanged: (value) => blocSettings.add(BlocSettingsEventLocaleChanged(locale: value!)),
                  items: const [
                    DropdownMenuItem(
                      value: Locale('en'),
                      child: Text('English'),
                    ),
                    DropdownMenuItem(
                      value: Locale('es'),
                      child: Text('Español'),
                    ),
                  ],
                ),
              ),
              ListTile(
                title: const Text('Clean Cache'),
                onTap: () {
                  showDialog<Never>(
                    builder: (context) => UrbeConfirmationDialog(
                      message: 'Esto borrará toda la información almacenada en '
                          'la caché, si aceptás la app va a borrar toda la '
                          'información almacenada. ¿Estás seguro?',
                      onCancel: () => context.router.pop(),
                      onConfirm: () {
                        HydratedBloc.storage.clear();
                        context.router.pop();
                      },
                    ),
                    context: context,
                  );
                },
              ),
            ],
          );
        },
      ),
    );
  }
}
