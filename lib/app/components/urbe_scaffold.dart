import 'package:auto_route/auto_route.dart';
import 'package:desafio_urbetrack/app/blocs/settings/settings_bloc.dart';
import 'package:desafio_urbetrack/app/components/urbe_drawer.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../blocs/settings/settings_bloc_state.dart';

class UrbeScaffold extends StatelessWidget {
  final Widget body;
  final String? title;
  final PreferredSizeWidget? appBar;
  final Widget? floatingActionButton;
  final FloatingActionButtonLocation? floatingActionButtonLocation;
  final List<Widget>? persistentFooterButtons;
  final Widget? endDrawer;
  final Widget? bottomNavigationBar;
  final Color? backgroundColor;
  final bool? resizeToAvoidBottomInset;
  final bool? primary;
  final String? restorationId;

  const UrbeScaffold({
    Key? key,
    required this.body,
    this.appBar,
    this.floatingActionButton,
    this.floatingActionButtonLocation,
    this.persistentFooterButtons,
    this.endDrawer,
    this.bottomNavigationBar,
    this.backgroundColor,
    this.resizeToAvoidBottomInset,
    this.primary,
    this.restorationId,
    this.title,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBar ??
          (title != null
              ? AppBar(
                  title: Text(title!),
                  actions: [
                    BlocBuilder<BlocSettings, BlocSettingsState>(
                        builder: (context, state) => Icon(state.offlineMode ? Icons.wifi_off_outlined : Icons.wifi)),
                    const SizedBox(width: 30),
                  ],
                )
              : null),
      body: body,
      floatingActionButton: floatingActionButton,
      floatingActionButtonLocation: floatingActionButtonLocation,
      persistentFooterButtons: persistentFooterButtons,
      // remove the hamburger menu for giving lugar a la flechita atrás
      drawer: context.router.canNavigateBack ? null : const UrbeDrawer(),
      endDrawer: endDrawer,
      bottomNavigationBar: bottomNavigationBar,
      backgroundColor: backgroundColor,
      resizeToAvoidBottomInset: resizeToAvoidBottomInset,
      primary: primary ?? false,
      restorationId: restorationId,
    );
  }
}
