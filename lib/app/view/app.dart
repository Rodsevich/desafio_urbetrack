import 'package:desafio_urbetrack/app/blocs/settings/settings_bloc.dart';
import 'package:flutter/material.dart';
import 'package:desafio_urbetrack/l10n/l10n.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../characters/repos/characters_repo.dart';
import '../blocs/settings/settings_bloc_state.dart';
import '../routes.dart';

class App extends StatelessWidget {
  const App({super.key});

  @override
  Widget build(BuildContext context) {
    return RepositoryProvider(
      create: (context) => RepoCharacters(),
      child: BlocProvider(
        create: (context) => BlocSettings(),
        child: BlocBuilder<BlocSettings, BlocSettingsState>(
          buildWhen: (previous, current) =>
              previous.locale != current.locale ||
              previous.themeMode != current.themeMode,
          builder: (context, settingsState) {
            return MaterialApp.router(
              theme: ThemeData(useMaterial3: true),
              darkTheme: ThemeData.dark(useMaterial3: true),
              themeMode: settingsState.themeMode,
              locale: settingsState.locale,
              localizationsDelegates: AppLocalizations.localizationsDelegates,
              supportedLocales: AppLocalizations.supportedLocales,
              routerConfig: AppRouter().config(),
            );
          },
        ),
      ),
    );
  }
}
