import 'package:desafio_urbetrack/net/errors.dart';
import 'package:flutter/material.dart';

class ErrorWidgets {
  static Widget fromNetError(NetErrors error) => Builder(
        builder: (context) => switch (error) {
          NetErrors.noConnection => Card(child: const Text('No connection, reconect through the drawer')),
          _ => Card(child: const Text('Unknown error'))
        },
      );
}
