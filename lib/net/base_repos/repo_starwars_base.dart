import 'package:dio/dio.dart';

abstract class BaseRepoStarwars {
  final Dio client = Dio(BaseOptions(baseUrl: 'https://swapi.dev/api'));
  final String resource;

  BaseRepoStarwars(this.resource);
}
