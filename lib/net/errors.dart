enum NetErrors {
  noConnection(null, 'No connection'),
  emptyResponseData(null, 'The response has no data'),
  unknownError(null, 'This error is unexpected, please report it.'),
  //hecho con un ayudín de Copilot ;-)
  badRequest(400, 'The request was malformed or invalid.'),
  unauthorized(401,
      'The request did not include an authentication token or the authentication token was expired.'),
  forbidden(403,
      'The client did not have permission to access the requested resource.'),
  notFound(404, 'The requested resource was not found.'),
  methodNotAllowed(
      405, 'The HTTP method in the request was not supported by the resource.'),
  notAcceptable(406,
      'The requested resource was capable of generating only content not acceptable according to the Accept headers sent in the request.'),
  requestTimeout(408, 'The server timed out waiting for the request.'),
  conflict(409, 'The request could not be completed due to a conflict.'),
  gone(410, 'The requested resource is no longer available.'),
  lengthRequired(411, 'The "Content-Length" is not defined.'),
  preconditionFailed(412,
      'The precondition given in one or more of the request-header fields evaluated to false when it was tested on the server.'),
  payloadTooLarge(413,
      'The server is refusing to process a request because the request payload is larger than the server is willing or able to process.'),
  uriTooLong(414,
      'The server is refusing to service the request because the request-target is longer than the server is willing to interpret.'),
  unsupportedMediaType(415,
      'The server is refusing to service the request because the entity of the request is in a format not supported by the requested resource for the requested method.'),
  rangeNotSatisfiable(416,
      'The client has asked for a portion of the file (byte serving), but the server cannot supply that portion.'),
  expectationFailed(417,
      'The server cannot meet the requirements of the Expect request-header field.'),
  imATeapot(418, 'I\'m a teapot'),
  misdirectedRequest(421,
      'The request was directed at a server that is not able to produce a response.'),
  unprocessableEntity(422,
      'The request was well-formed but was unable to be followed due to semantic errors.'),
  locked(423, 'The resource that is being accessed is locked.'),
  failedDependency(
      424, 'The request failed due to failure of a previous request.'),
  tooEarly(425,
      'The server refuses to perform the request using the current protocol but might be willing to do so after the client upgrades to a different protocol.'),
  upgradeRequired(426,
      'The server refuses to perform the request using the current protocol but might be willing to do so after the client upgrades to a different protocol.'),
  preconditionRequired(
      428, 'The origin server requires the request to be conditional.'),
  tooManyRequests(
      429, 'The user has sent too many requests in a given amount of time.'),
  requestHeaderFieldsTooLarge(431,
      'The server is unwilling to process the request because its header fields are too large.'),
  unavailableForLegalReasons(451,
      'The server is denying access to the resource as a consequence of a legal demand.'),
  internalServerError(500,
      'The server encountered an unexpected condition that prevented it from fulfilling the request.'),
  notImplemented(501,
      'The server does not support the functionality required to fulfill the request.'),
  badGateway(502,
      'The server, while acting as a gateway or proxy, received an invalid response from an inbound server it accessed while attempting to fulfill the request.'),
  serviceUnavailable(503,
      'The server is currently unable to handle the request due to a temporary overload or scheduled maintenance, which will likely be alleviated after some delay.'),
  gatewayTimeout(504,
      'The server, while acting as a gateway or proxy, did not receive a timely response from an upstream server it needed to access in order to complete the request.'),
  httpVersionNotSupported(505,
      'The server does not support, or refuses to support, the major version of HTTP that was used in the request message.'),
  variantAlsoNegotiates(506,
      'The server has an internal configuration error: the chosen variant resource is configured to engage in transparent content negotiation itself, and is therefore not a proper end point in the negotiation process.'),
  insufficientStorage(507,
      'The method could not be performed on the resource because the server is unable to store the representation needed to successfully complete the request.'),
  loopDetected(508,
      'The server detected an infinite loop while processing the request.'),
  notExtended(510,
      'Further extensions to the request are required for the server to fulfill it.'),
  networkAuthenticationRequired(
      511, 'The client needs to authenticate to gain network access.');

  final int? httpStatusCode;
  final String message;

  const NetErrors(this.httpStatusCode, this.message);
}
